// NPM
var express = require('express');
var multer  = require('multer');
var path = require('path');

// Initialise App
var app = express();
var upload = multer({ dest: 'uploads/' });

// Varialbles
var port = process.env.PORT || 8080;

app.get('/', function(req, res) {
  var fileName = path.join(__dirname, 'index.html');
  
  // Sends the file
  res.sendFile(fileName, function (err) {
      
    // Handle Error
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    else {
      console.log('Sent:', fileName);
    }
  });
});


app.post('/size', upload.single('file'), function (req,res) {
  res.send({size: req.file.size});
});

// Listen
app.listen(port, function(){
    console.log("Listning on port " + port);
})