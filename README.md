# Request Header Parser Microservice (FreeCodeCamp)
This is the backend project for FreeCodeCamp - See [Here](https://www.freecodecamp.com/challenges/file-metadata-microservice)

## Usage
Go to [the Heroku App](filemetadataservices.herokuapp.com) and follow the instructions!

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## User Storys
 I can submit a FormData object that includes a file upload.
 When I submit something, I will receive the file size in bytes within the JSON response.

## License
Released under GNU (General Public License)
Permission for individuals, Organizations and Companies to run, study, share (copy), and modify the software and its source.
Copyright Adam Kelly 2016-2017